import React, {Component} from 'react';
// import List from './components/List/List';
import Joke from './components/Joke/Joke';
import axios from './axios';
import withLoader from "./hoc/withLoader/withLoader";
import Spinner from './components/UI/Spinner/Spinner';


import './App.css';

class App extends Component {

    state = {
        joke: {},
        loading: true
    };

    requestJoke = () => {

        axios.get('random').then(response => {
            const newJoke = {value: response.data.value, id: response.data.id};
            this.setState({joke: newJoke});

        }).finally(() => {
            this.setState({loading: false})
        });
    };

    newJoke = (event) => {
        event.preventDefault();
        this.setState({loading: true});
        this.requestJoke();
    };

    componentDidMount() {
        this.requestJoke();
    }

    render() {
        let content = null;

        if (this.state.loading) {
            content = <Spinner/>
        } else {
            content = <Joke joke={this.state.joke}/>
        }

        return (
            <div className="App">
                {content}
                <button className="New-Joke-Button" onClick={this.newJoke}>NEW JOKE</button>
            </div>
        );
    }
}

export default withLoader(App, axios);
