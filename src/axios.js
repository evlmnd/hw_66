import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://api.chucknorris.io/jokes/'
});

instance.interceptors.request.use(req => {
    return req;
});

export default instance;