import React from 'react';

import './Joke.css';

const Joke = (props) => {
    return (
        <div className="Joke">
            {props.joke.value}
        </div>
    );
};

export default Joke;